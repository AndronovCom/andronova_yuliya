<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAbout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('marital',100);
            $table->string('children',100);
            $table->string('english',100);
            $table->string('phone',20);
            $table->string('email',50);
            $table->string('url',50);
            $table->string('Adress',100);
            $table->string('services',100);
            $table->string('img',255);
            $table->string('facebook', 255);
            $table->string('icon', 255);
            $table->text('text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about');
    }
}
