<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Schema\Blueprint;
use App\Models\EducationModel;

class EducationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Schema::hasTable('education') && EducationModel::count()==0)
        {
            EducationModel::insert([
                [
                    'title' => 'Самообразование',
                    'name'  => 'Сентябрь 2018 - по настоящее время',
                    'text'  => 'Из-за отстутствия возможности продолжать обучение в КА"ШАГ" было принято решение изучать технологии самостоятельно. На данный момент ознакомилась с языком программирования PHP, CMS WordPress, фреймворк Laravel'
                ],[
                    'title' => 'Обучение',
                    'name'  => 'КА "ШАГ" - 2017 - 2018',
                    'text'  => 'Профессиональное компьютерное образование - Разработка программного обеспечения'
                ],[
                    'title' => 'Диплом специалиста',
                    'name'  => 'ХНЕУ ім. С. Кузнеця - 2004 - 2010',
                    'text'  => 'специальность "Информационные управляющие системы и технологии"'
                ]
            ]);
        }
    }
}