<?php

use Illuminate\Database\Seeder;
use App\Models\SocialModel;

class SocialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Schema::hasTable('socials') && SocialModel::count()==0)
        {
            SocialModel::insert([
                [
                    'alias'     => 'facebook',
                    'icon'      => 'icon-facebook2'
                ],[
                    'alias'     => 'bitbucket',
                    'icon'      => 'icon-bitbucket',
                    'url'       => 'https://bitbucket.org/AndronovCom/andronova_yuliya/src/master/'
                ]
            ]);
        }

    }
}
