<?php

use Illuminate\Database\Seeder;
use App\Models\AboutModel;

class AboutTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (AboutModel::count()==0){
            AboutModel::create([
                'name'      => 'Андронова Юлия Игоревна',
                'phone'     => '063 354 73 77',
                'marital'   => 'замужем',
                'children'  => 'две дочки (5 и 7 лет)',
                'english'   => 'pre-intermediate',
                'email'     => 'slastin-julija@meta.ua',
                'url'       => 'http://andronova-yuliya.club',
                'Adress'    => 'Харьков',
                'services'  => 'Web Developer/BackEnd',
                'img'       => 'user-3.jpg',
                'facebook'  => 'https://www.facebook.com/slastin.julija',
                'icon'      => 'icon-facebook2',
                'text'      => '<p>Во время обучения в КА"ШАГ" к выполнению домашнего задания относилась ответственно. Достижением считаю высокие показатели на экзаменах, многие из которых были сданы "автоматом" на 12 баллов. Имеется большое желание совершенствовать свои навыки.</p><p> Из личных качеств хочу отметить желание работать в команде, усидчивость, упорность, отзывчивость.</p><p>Данный сайт был написан в процессе ознакомления с фреймворком Laravel.</p>',
            ]);
        }
    }
}
