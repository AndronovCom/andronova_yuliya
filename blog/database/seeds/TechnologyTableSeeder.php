<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Schema\Blueprint;
use App\Models\TechnologyModel;

class TechnologyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Schema::hasTable('technology') && TechnologyModel::count()==0)
        {
            TechnologyModel::insert([
                [
                    'name'  => 'HTML5',
                    'icon'  => 'icon-html-five'
                ],[
                    'name'  => 'CSS3',
                    'icon'  => 'icon-css3'
                ],[
                    'name'  => 'jQuery',
                    'icon'  => 'icon-jquery'
                ],[
                    'name'  => 'PHP',
                    'icon'  => 'icon-php'
                ],[
                    'name'  => 'MySQL',
                    'icon'  => 'icon-mysql'
                ],[
                    'name'  => 'JS',
                    'icon'  => 'icon-javascript'
                ],[
                    'name'  => 'Laravel',
                    'icon'  => 'icon-laravel'
                ],[
                    'name'  => 'C/C++',
                    'icon'  => ''
                ],[
                    'name'  => 'Git',
                    'icon'  => 'icon-git'
                ],[
                    'name'  => 'WinApi',
                    'icon'  => ''
                ],[
                    'name'  => 'C#',
                    'icon'  => ''
                ],[
                    'name'  => 'WordPress',
                    'icon'  => 'icon-wordpress'
                ]
            ]);
        }
    }
}
