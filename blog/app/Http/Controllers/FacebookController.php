<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class FacebookController extends Controller
{
    private $access_token="https://graph.facebook.com/v3.2/me?fields=id%2Cname%2Cposts%7Bdescription%2Cpicture%2Clink%2Ccreated_time%7D&access_token=EAAbRZA4Xg0R0BACedkIqAfsO9c0Db5eLc1Pa70OcDENdiZC1XmQsiGh4R4usmr7OrOz7hGsYvZC36rtCbL7kt5iLZCZB4qHXHZBGRsSpzByWLEDxDbfwYZBrUdghwihiZBbEBVA05syEIkqJFfvMBONjeXqHmNqJ5vQmkfn3xEMnstPdWfJEmfCd";
    private $json;
    private $obj;

    public function execute(Request $request)
    {
        $this->getJson();
        $this->decodeJson();
        $this->time_string();
        $this->obj_filter();
        return view('site.posts',['posts'=>$this->obj]);
    }

    private function getJson(){
        try{
        $this->json = file_get_contents($this->access_token);
        }
        catch (Exception $exception){
            $this->json =null;
        }
    }

    private function decodeJson(){
        $this->obj = json_decode($this->json, true);
        $this->obj=$this->obj['posts']['data'];
    }

    private function time_string(){
        foreach ($this->obj as &$value)
        {
            $value['created_time']=date('d.m.Y', strtotime( $value['created_time']));
        }
    }

    private function obj_filter(){
        $this->obj=array_filter($this->obj, function ($item){
            return (isset($item['description']));
        },ARRAY_FILTER_USE_BOTH );
    }
}
