<?php

namespace App\Http\Controllers;

use App\Models\AboutModel;
use App\Models\EducationModel;
use App\Models\TechnologyModel;
use App\Models\SocialModel;


class IndexController extends Controller
{
    public function execute(){
        $about=AboutModel::get(['name','marital','children','english','phone', 'email', 'url', 'Adress', 'services', 'img', 'text']);
        $educations=EducationModel::get(['title', 'name', 'text']);
        $technologies=TechnologyModel::get(['name', 'icon']);
        $socials=SocialModel::get(['alias', 'icon', 'url']);
        return view('site.index', ['about'=>$about, 'educations'=>$educations, 'technologies'=>$technologies, 'socials'=>$socials]);
    }
}
