<!DOCTYPE HTML>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'Андронова Юлия') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="CV Andronova" />
    <meta name="keywords" content="cv rezume php laravel trainee junior" />
    <meta name="author" content="freehtml5.co" />

    <!--
    //////////////////////////////////////////////////////

    FREE HTML5 TEMPLATE
    DESIGNED & DEVELOPED by FreeHTML5.co

    Website: 		http://freehtml5.co/
    Email: 			info@freehtml5.co
    Twitter: 		http://twitter.com/fh5co
    Facebook: 		https://www.facebook.com/fh5co

    //////////////////////////////////////////////////////
     -->

    <!-- Facebook and Twitter integration -->
    <meta property="og:type" conyent="website">
    <meta property="og:title" content="Andronova CV"/>
    <meta property="og:url" content="http://andronova-yuliya/"/>
    <meta property="og:site_name" content="Andronova-Yuliya"/>
    <meta property="og:description" content="CV"/>

    <link href="https://fonts.googleapis.com/css?family=Space+Mono" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{asset('assets/css/icomoon.css')}}">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">

    <!-- Theme style  -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

    <!-- Modernizr JS -->
    <script src="{{asset('assets/js/modernizr-2.6.2.min.js')}}"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="{{asset('assets/js/respond.min.js')}}"></script>
    <![endif]-->

</head>
<body>
<script src="{{asset('assets/js/facebookSDK.js')}}"></script>

<div class="fh5co-loader"></div>

<div id="page">
        <!--Header-->
        @yield('header')

    <!--Content-->
    @yield('content')
</div>

<!--Footer-->
<div id="fh5co-footer">
    @yield('footer')
</div>

<!--Gototop-->
<div class="gototop js-top">
    @yield('gototop')
</div>

<!-- jQuery -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- jQuery Easing -->
<script src="{{asset('assets/js/jquery.easing.1.3.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<!-- Waypoints -->
<script src="{{asset('assets/js/jquery.waypoints.min.js')}}"></script>
<!-- Stellar Parallax -->
<script src="{{asset('assets/js/jquery.stellar.min.js')}}"></script>
<!-- Easy PieChart -->
<script src="{{asset('assets/js/jquery.easypiechart.min.js')}}"></script>
<!-- Google Map -->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>-->
<!--<script src="js/google_map.js"></script>-->

<!-- Main -->
<script src="{{asset('assets/js/main.js')}}"></script>

</body>
</html>

