<div id="fh5co-about" class="animate-box">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                <h2>Обо мне</h2>
            </div>
        </div>
        <div class="row">
            @if(isset($about))
                <div class="col-md-4">
                    <ul class="info">
                        <li><span class="first-block">Полное Имя:</span><span class="second-block">{{$about[0]->name}}</span></li>
                        <li><span class="first-block">Семейное положение:</span><span class="second-block">{{$about[0]->marital}}</span></li>
                        <li><span class="first-block">Дети:</span><span class="second-block">{{$about[0]->children}}</span></li>
                        <li><span class="first-block">Английский:</span><span class="second-block">{{$about[0]->english}}</span></li>
                        <li><span class="first-block">Телефон:</span><a href="tel:{{$about[0]->phone}}"><span class="second-block">{{$about[0]->phone}}</span></a></li>
                        <li><span class="first-block">Email:</span><a href="mailto:{{$about[0]->email}}"><span class="second-block">{{$about[0]->email}}</span></a></li>
                        <li><span class="first-block">Сайт:</span><a href="{{$about[0]->url}}"><span class="second-block">{{$about[0]->url}}</span></a></li>
                        <li><span class="first-block">Город:</span><span class="second-block">{{$about[0]->Adress}}</span></li>
                    </ul>
                </div>
                <div class="col-md-8">
                    {!!$about[0]->text!!}
                </div>
            @endif
        </div>
    </div>
</div>

<div id="fh5co-resume" class="fh5co-bg-color">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                <h2>Резюме</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <ul class="timeline">

                    <li class="timeline-heading text-center animate-box">
                        <div><h3>Образование</h3></div>
                    </li>
                @if(isset($educations))
                    @foreach($educations as $count=>$education)
                        @if($count%2==0)
                            <li class="timeline-inverted animate-box">
                                <div class="timeline-badge"><i class="icon-graduation-cap"></i></div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h3 class="timeline-title">{{$education->title}}</h3>
                                        <span class="company">{{$education->name}}</span>
                                    </div>
                                    <div class="timeline-body">
                                        <p>{{$education->text}}</p>
                                    </div>
                                </div>
                            </li>
                        @else
                            <li class="animate-box timeline-unverted">
                                <div class="timeline-badge"><i class="icon-graduation-cap"></i></div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h3 class="timeline-title">{{$education->title}}</h3>
                                        <span class="company">{{$education->name}}</span>
                                    </div>
                                    <div class="timeline-body">
                                        <p>{{$education->text}}</p>
                                    </div>
                                </div>
                            </li>
                        @endif
                    @endforeach
                @endif

                </ul>
            </div>
        </div>
    </div>
</div>

<div id="fh5co-skills" class="animate-box">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                <h2>Технологии, с которыми я ознакомилась за прошедший год</h2>
            </div>
        </div>

        @if(isset($educations))
            <div class="row row-pb-md">
                @foreach($technologies as$count=>$technology)
                    <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                        <div class="chart" data-percent="100"><span><strong>{{$technology->name}}<br /><i class="{{$technology->icon}}"></i></strong></span></div>
                    </div>
                @endforeach
            </div>
        @endif

    </div>
</div>


