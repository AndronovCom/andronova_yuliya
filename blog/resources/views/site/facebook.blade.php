<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
            <a href="/"><button  type="button" class="btn btn-primary">На главную</button></a>
            <a href="https://www.facebook.com/slastin.julija"><button  type="button" class="btn btn-primary">Facebook</button></a>
        </div>
    </div>
    @if(isset($posts))
        <div class="row row-pb-md">
            @foreach($posts as $post)
                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                    <div class="text-center">{{$post['created_time']}}
                    </div>
                    @if(isset($post['picture']))
                        <img class="card-img-top" src="{{$post['picture']}}" alt="Card image cap">
                    @endif
                    <div class="card-body">
                        <a href="{{$post['link']}}" class="">
                            @if(isset($post['description']))
                                <h5 class="card-title">{{$post['description']}}</h5>
                            @endif</a>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>