<header id="fh5co-header" class="fh5co-cover js-fullheight" role="banner" style="background-image:url({{asset('assets/images/cover_bg_3.jpg')}});" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <div class="display-t js-fullheight">
                    <div class="display-tc js-fullheight animate-box" data-animate-effect="fadeIn">
                        <div class="profile-thumb" style="background-image: url({{asset('assets/images/'.$about[0]->img)}});"></div>
                        @if(isset($about))
                            @foreach($about as $item)
                                <h1><span>{{$item['name']}}</span></h1>
                                <h3><span>{{$item['services']}}</span></h3>
                            @endforeach
                        @endif
                        @if(isset($socials))
                            <p>
                                <ul class="fh5co-social-icons">
                                @foreach($socials as $social)
                                    @if(isset($social->url))
                                        <li><a href="{{$social->url}}"><i class="{{$social->icon}}"></i></a></li>
                                        @else
                                        <li><a href="{{ route('facebook')}}"><i class="{{$social->icon}}"></i></a></li>
                                    @endif
                                @endforeach
                                </ul>
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>